import boto3
import logging
import os
import zipfile
from botocore.exceptions import ClientError
from use_case_2 import constants
from use_case_2.create_stack import create, delete_bucket


s3_client = boto3.client('s3', region_name=constants.region)
template_url = f"https://{constants.host_bucket}.s3.{constants.region}.amazonaws.com/{constants.temp_name}"
parameters = [dict(ParameterKey="HostBucket", ParameterValue=constants.host_bucket),
              dict(ParameterKey="SourceBucket", ParameterValue=constants.source_bucket),
              dict(ParameterKey="DestBucket", ParameterValue=constants.dest_bucket)]


def execute_script():
    try:
        location = {'LocationConstraint': constants.region}
        s3_client.create_bucket(Bucket=constants.host_bucket, CreateBucketConfiguration=location)
        s3_client.upload_file(constants.temp_name, constants.host_bucket, constants.temp_name)
        upload_zip(constants.host_bucket, "copy_to_s3_function.py", "copy_to_s3_function.zip")
        return True
    except ClientError as e:
        delete_bucket(constants.host_bucket)
        print(f"Error: {e} occur")
        return False


def upload_zip(bucket_name, input_filename, output_filename):
    zip_file = zipfile.ZipFile(output_filename, "w")
    zip_file.write(input_filename, os.path.basename(input_filename))
    zip_file.close()
    s3_client.upload_file(output_filename, bucket_name, output_filename)
    os.remove(output_filename)


if __name__ == "__main__":
    response = execute_script()
    if response:
        create(constants.stack_name, template_url, parameters)
        s3_client.put_object(Body='hello', Key=constants.key_name, Bucket=constants.source_bucket)
    else:
        print('Stack Creation Failed')

