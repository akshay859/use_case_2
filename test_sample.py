import xmlrunner
import unittest


class SimpleTest(unittest.TestCase):

    # Returns True or False.
    def test(self):
        self.assertEqual(10, 11)


if __name__ == '__main__':
    with open('results.xml', 'wb') as output:
        unittest.main(testRunner=xmlrunner.XMLTestRunner(output=output), failfast=False, buffer=False, catchbreak=False)
