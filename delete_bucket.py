import boto3


bucket_name = 'use-case-2-01012020-source'


def delete_bucket(bucket_name):
    s3 = boto3.resource('s3')
    bucket = s3.Bucket(bucket_name)
    for key in bucket.objects.all():
        key.delete()
    bucket.delete()


delete_bucket(bucket_name)